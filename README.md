# KIWI

Building Images using kiwi

## Prepare kiwi machine (Ubuntu 20.04)
```
pip install kiwi
apt -y install debootstrap
apt -y install qemu-utils
apt -y install mtools 
apt -y install xorriso
```

## Download descriptions
```
git clone https://github.com/OSInside/kiwi-descriptions.git
```

## Build Image
```
kiwi-ng system build --description kiwi-descriptions/ubuntu/x86_64/ubuntu-bionic/ --target-dir /tmp/my-ubuntu-image
```

## Hints
``` 
openssl passwd -5 -salt xyz root
```

## Mato
```
1. download ubuntu
2. mount it to local machine
3. copy stuff into a new folder
4. modify
5. adjust configs (nocloud, grub)
6. chorriso command
```
